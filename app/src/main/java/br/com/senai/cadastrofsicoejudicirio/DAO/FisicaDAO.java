package br.com.senai.cadastrofsicoejudicirio.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.cadastrofsicoejudicirio.Modelo.Fisica;

/**
 * Created by 47501098867 on 28/02/2018.
 */

public class FisicaDAO extends SQLiteOpenHelper {

    public FisicaDAO(Context context){
        super(context, "Listagem pessoas físicas", null, 1);}

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE PessoaF (id INTEGER PRIMARY KEY, nome TEXT, cpf TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS PessoaF";
        db.execSQL(sql);
    }

    public void inserir(Fisica fisica){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();
        dados.put("nome", fisica.getNome());
        dados.put("cpf", fisica.getCpf());

        db.insert("PessoaF", null,dados);
    }

    public List<Fisica> buscaPessoasF(){
        String sql = "SELECT * FROM PessoaF";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Fisica> pessoasF = new ArrayList<>();
        while (c.moveToNext()){
            Fisica fisica = new Fisica();
            fisica.setId(c.getLong(c.getColumnIndex("id")));
            fisica.setNome(c.getString(c.getColumnIndex("nome")));
            fisica.setCpf(c.getString(c.getColumnIndex("cpf")));

            pessoasF.add(fisica);
        }
        return pessoasF;
    }
}
