package br.com.senai.cadastrofsicoejudicirio.Listas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import br.com.senai.cadastrofsicoejudicirio.DAO.JuridicaDAO;
import br.com.senai.cadastrofsicoejudicirio.Modelo.Juridica;
import br.com.senai.cadastrofsicoejudicirio.R;

public class ListaPessoasJuridicas extends AppCompatActivity {

    private ListView listaJuridica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pessoas_juridicas);

        listaJuridica = findViewById(R.id.listJuridica);

        carregarLista();
    }

    private void carregarLista(){
        JuridicaDAO dao = new JuridicaDAO(this);
        List<Juridica> pessoasj = dao.buscaPessoasJ();

        ArrayAdapter<Juridica> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, pessoasj);
        listaJuridica.setAdapter(adapter);
    }
}
