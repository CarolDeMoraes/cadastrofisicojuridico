package br.com.senai.cadastrofsicoejudicirio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import br.com.senai.cadastrofsicoejudicirio.Listas.ListaPessoasFisicas;
import br.com.senai.cadastrofsicoejudicirio.Listas.ListaPessoasJuridicas;

public class MainActivity extends AppCompatActivity {
    private Button botaoCadastro;
    private Button botaoListaF;
    private Button botaoListaJ;
    private CheckBox checkFisica;
    private CheckBox checkJuridica;
    private EditText nome;
    private TextView textoErros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botaoCadastro = findViewById(R.id.btnCadastro);
        checkFisica = findViewById(R.id.cbFisica);
        checkJuridica = findViewById(R.id.cbJuridica);
        nome = findViewById(R.id.txtNome);
        textoErros = findViewById(R.id.tvErro);

        botaoListaF = findViewById(R.id.btnFis);
        botaoListaJ = findViewById(R.id.btnJur);

        botaoCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkJuridica.isChecked() && checkFisica.isChecked()){
                    textoErros.setText("Escolha somente um por favor");}
                else if (checkFisica.isChecked()){
                    Intent intent = new Intent(MainActivity.this, FormularioFisica.class);
                    intent.putExtra("nome", nome.getText().toString());
                    startActivity(intent);
                }else if (checkJuridica.isChecked()) {
                    Intent intent = new Intent(MainActivity.this, FormularioJuridica.class);
                    intent.putExtra("nome", nome.getText().toString());
                    startActivity(intent);
                }
                else{
                    textoErros.setText("Escolha um");
                }
            }
        });

        botaoListaF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ListaPessoasFisicas.class);
                startActivity(intent);
            }
        });

        botaoListaJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent (MainActivity.this, ListaPessoasJuridicas.class);
                startActivity(intent);
            }
        });
    }
}
