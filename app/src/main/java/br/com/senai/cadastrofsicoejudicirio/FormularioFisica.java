package br.com.senai.cadastrofsicoejudicirio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.senai.cadastrofsicoejudicirio.DAO.FisicaDAO;
import br.com.senai.cadastrofsicoejudicirio.Modelo.Fisica;

public class FormularioFisica extends AppCompatActivity {

    private EditText campoNome;
    private EditText campoCpf;
    private Button botaoCadastra;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_fisica);

        campoNome = findViewById(R.id.txtNome);
        campoCpf = findViewById(R.id.txtCnpj);
        botaoCadastra = findViewById(R.id.btnCadastro);
        Bundle extras = getIntent().getExtras();
        String nome = extras.getString("nome");

        campoNome.setText(nome);


        botaoCadastra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fisica fisica =  pegaPessoaF();
                FisicaDAO dao = new FisicaDAO(FormularioFisica.this);
                dao.inserir(fisica);
                dao.close();
                Toast.makeText(getApplicationContext(),"Pessoa: "+ fisica.getNome()+" cadastrado(a)",
                Toast.LENGTH_LONG).show();
                finish();

            }
        });
    }

        public Fisica pegaPessoaF(){
            Fisica fisica = new Fisica();
            fisica.setCpf(campoCpf.getText().toString());
            fisica.setNome(campoNome.getText().toString());
            return fisica;
        }
    }
