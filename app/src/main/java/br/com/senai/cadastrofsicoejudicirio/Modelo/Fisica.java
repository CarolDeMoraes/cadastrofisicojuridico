package br.com.senai.cadastrofsicoejudicirio.Modelo;

/**
 * Created by 47501098867 on 28/02/2018.
 */

public class Fisica {
    private Long id;
    private String nome;
    private String cpf;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getId()+" "+ getNome();
    }
}
