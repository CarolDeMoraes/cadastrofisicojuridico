package br.com.senai.cadastrofsicoejudicirio;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.senai.cadastrofsicoejudicirio.DAO.JuridicaDAO;
import br.com.senai.cadastrofsicoejudicirio.Modelo.Fisica;
import br.com.senai.cadastrofsicoejudicirio.Modelo.Juridica;

public class FormularioJuridica extends AppCompatActivity {

    private EditText nome;
    private EditText cnpj;
    private Button botaoCadastro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_juridica);

        nome = findViewById(R.id.txtNome);
        cnpj = findViewById(R.id.txtCnpj);
        botaoCadastro = findViewById(R.id.btnCadastro);
        Bundle extras = getIntent().getExtras();
        String textoNome = extras.getString("nome");

        nome.setText(textoNome);

        botaoCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Juridica juridica = pegaPessoaJ();
                JuridicaDAO dao = new JuridicaDAO(FormularioJuridica.this);
                dao.inserir(juridica);
                dao.close();
                Toast.makeText(getApplicationContext(),"Pessoa: "+juridica.getNome()+" cadastrado(a)",Toast.LENGTH_LONG)
                        .show();
                finish();

            }
        });

    }
        public Juridica pegaPessoaJ(){
           Juridica juridica = new Juridica();
           juridica.setNome(nome.getText().toString());
           juridica.setCnpj(cnpj.getText().toString());
           return juridica;
        }
    }

//IMPLEMENTA O DAO AGORA