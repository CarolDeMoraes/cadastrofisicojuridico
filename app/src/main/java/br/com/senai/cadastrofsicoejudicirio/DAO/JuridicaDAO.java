package br.com.senai.cadastrofsicoejudicirio.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.com.senai.cadastrofsicoejudicirio.Modelo.Juridica;

/**
 * Created by 47501098867 on 28/02/2018.
 */

public class JuridicaDAO extends SQLiteOpenHelper{
    public JuridicaDAO(Context context) {
        super(context, "Listagem Pessoas Jurídicas", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE PessoaJ (id INTEGER PRIMARY KEY, nome TEXT, cnpj TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String sql = "DROP TABLE IF EXISTS PessoaJ";
        db.execSQL(sql);
    }

    public void inserir(Juridica juridica){
        SQLiteDatabase db = getWritableDatabase();
        ContentValues dados = new ContentValues();
        dados.put("nome", juridica.getNome());
        dados.put("cnpj", juridica.getCnpj());

        db.insert("PessoaJ", null, dados);
    }

    public List<Juridica> buscaPessoasJ(){
        String sql = "SELECT * FROM PessoaJ";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);
        List<Juridica> pessoasJ = new ArrayList<>();
        while (c.moveToNext()){
            Juridica juridica= new Juridica();
            juridica.setId(c.getLong(c.getColumnIndex("id")));
            juridica.setNome(c.getString(c.getColumnIndex("nome")));
            juridica.setCnpj(c.getString(c.getColumnIndex("cnpj")));

            pessoasJ.add(juridica);
        }
        return pessoasJ;
    }
}
