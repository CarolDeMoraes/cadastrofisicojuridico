package br.com.senai.cadastrofsicoejudicirio.Modelo;

/**
 * Created by 47501098867 on 28/02/2018.
 */

public class Juridica {
    private Long id;
    private String nome;
    private String cnpj;

    public String getNome() {
        return nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    @Override
    public String toString() {
        return getId()+" "+ getNome();
    }
}
