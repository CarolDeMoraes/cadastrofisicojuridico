package br.com.senai.cadastrofsicoejudicirio.Listas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import br.com.senai.cadastrofsicoejudicirio.DAO.FisicaDAO;
import br.com.senai.cadastrofsicoejudicirio.Modelo.Fisica;
import br.com.senai.cadastrofsicoejudicirio.R;

public class ListaPessoasFisicas extends AppCompatActivity {

    private ListView listaFisica;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pessoas_fisicas);
        listaFisica = findViewById(R.id.listFisico);

        carregarLista();
    }

    private void carregarLista(){
        FisicaDAO dao = new FisicaDAO(this);
        List<Fisica> pessoasf = dao.buscaPessoasF();

        ArrayAdapter<Fisica> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, pessoasf);
        listaFisica.setAdapter(adapter);
    }



}
